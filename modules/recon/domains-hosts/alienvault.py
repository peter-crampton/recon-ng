from recon.core.module import BaseModule
import urllib
import json

class Module(BaseModule):

    meta = {
        'name': 'AlienVault OTX',
        'author': 'Peter Crampton <cramppet>',
        'description': 'Uses the AlienVault service to find subdomains in passive DNS data',
        'query': 'SELECT DISTINCT domain FROM domains WHERE domain IS NOT NULL',
    }

    def module_run(self, domains):
        for domain in domains:
            self.heading(domain, level=0)
            resp = self.request('https://otx.alienvault.com/api/v1/indicators/hostname/%s/passive_dns' % domain)
            if resp.status_code != 200:
                self.error('AlienVault: Recieved Non-200 HTTP Status Code => %d' % resp.status_code)
                break
            else:
                root = json.loads(resp.text)
                if root['count'] != 0:
                    for obj in root['passive_dns']:
                        host = obj['hostname'].lower()
                        self.add_hosts(host)


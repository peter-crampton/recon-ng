from recon.core.module import BaseModule
import urllib
import json

class Module(BaseModule):

    meta = {
        'name': 'VirusTotal',
        'author': 'Peter Crampton <cramppet>',
        'description': 'Uses the VirusTotal service to find subdomains in passive DNS data',
        'query': 'SELECT DISTINCT domain FROM domains WHERE domain IS NOT NULL',
        'required_keys': ['virustotal'],
    }

    def module_run(self, domains):
        apikey = self.get_key('virustotal')
        for domain in domains:
            self.heading(domain, level=0)
            query_str = urllib.urlencode({ 'domain': domain, 'apikey': apikey })
            resp = self.request('http://www.virustotal.com/vtapi/v2/domain/report?%s' % query_str)
            if resp.status_code != 200:
                self.error("VirusTotal: Recieved Non-200 HTTP Status Code => %d" % resp.status_code)
                break
            else:
                root = json.loads(resp.text)
                if 'subdomains' in root:
                    for host in root['subdomains']:
                        self.add_hosts(host)


from recon.core.module import BaseModule
from urlparse import urlparse
import urllib
import json
import re

class Module(BaseModule):

    meta = {
        'name': 'urlscan.io',
        'author': 'Peter Crampton <cramppet>',
        'description': 'Uses the Urlscan service to find subdomains in passive DNS data',
        'query': 'SELECT DISTINCT domain FROM domains WHERE domain IS NOT NULL',
    }

    def module_run(self, domains):
        for domain in domains:
            self.heading(domain, level=0)
            query_str = urllib.urlencode({ 'q': 'page.domain:%s' % domain })
            resp = self.request('https://urlscan.io/api/v1/search/?%s' % query_str)
            if resp.status_code != 200:
                self.error('Urlscan: Recieved Non-200 HTTP Status Code => %d' % resp.status_code)
                break
            else:
                root = json.loads(resp.text)
                if root['total'] != 0:
                    # Urlscan gives us two different URLs to examine:
                    # The original user-submitted URL and the final URL after any redirection
                    for resobj in root['results']:
                        if 'url' in resobj['task']:
                            ori = urlparse(resobj['task']['url']).netloc.lower()
                            if re.match(r'[a-zA-Z0-9\-\_]+\.%s' % domain, ori):
                                self.add_hosts(ori)

                        if 'domain' in resobj['page']:
                            final = resobj['page']['domain'].lower()
                            if re.match(r'[a-zA-Z0-9\-\_]+\.%s' % domain, final):
                                self.add_hosts(final)


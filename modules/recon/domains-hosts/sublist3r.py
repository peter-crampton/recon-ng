from recon.core.module import BaseModule
import urllib
import json

class Module(BaseModule):

    meta = {
        'name': 'Sublist3r',
        'author': 'Peter Crampton <cramppet>',
        'description': 'Uses the Sublist3r service to find subdomains in passive DNS data',
        'query': 'SELECT DISTINCT domain FROM domains WHERE domain IS NOT NULL',
    }

    def module_run(self, domains):
        for domain in domains:
            self.heading(domain, level=0)
            query_str = urllib.urlencode({ 'domain': domain })
            resp = self.request('https://api.sublist3r.com/search.php?%s' % query_str)
            if resp.status_code != 200:
                self.error('Sublist3r: Recieved Non-200 HTTP Status Code => %d' % resp.status_code)
                break
            else:
                root = json.loads(resp.text)
                for host in root:
                    self.add_hosts(host)


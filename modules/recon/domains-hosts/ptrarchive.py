from recon.core.module import BaseModule
import urllib
import re

class Module(BaseModule):

    meta = {
        'name': 'PTRArchive',
        'author': 'Peter Crampton <cramppet>',
        'description': 'Uses the PTRArchive service to find subdomains in passive DNS data',
        'query': 'SELECT DISTINCT domain FROM domains WHERE domain IS NOT NULL',
    }

    def module_run(self, domains):
        for domain in domains:
            self.heading(domain, level=0)
            query_str = urllib.urlencode({ 'label': domain, 'date': 'ALL' })
            resp = self.request(url='http://ptrarchive.com/tools/search2.htm?%s' % query_str)

            if resp.status_code != 200:
                self.error("PTRArchive: Recieved Non-200 HTTP Status Code => %d" % resp.status_code)
                break
            else:
                for host in re.findall(r'[a-z0-9\.\-\_]+\.%s' % domain, resp.text):
                    self.add_hosts(host)

